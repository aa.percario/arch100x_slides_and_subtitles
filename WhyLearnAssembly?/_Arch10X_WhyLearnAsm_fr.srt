0
00:00:00,560 --> 00:00:04,560
Salut les amis ! Avant de démarrer

1
00:00:02,960 --> 00:00:06,480
Cette grande aventure, peut-être que vous souhaiteriez

2
00:00:04,560 --> 00:00:08,080
savoir pourquoi nous pensons, chez OST, que vous devriez

3
00:00:06,480 --> 00:00:09,920
apprendre l'assembleur.

4
00:00:08,080 --> 00:00:12,000
Commençons avec les raisons simples. La

5
00:00:09,920 --> 00:00:13,840
première est que contrairement à d'autres compétences

6
00:00:12,000 --> 00:00:15,440
comme celles en sécurité des réseaux,

7
00:00:13,840 --> 00:00:16,960
les connaissances en langage assembleur sont

8
00:00:15,440 --> 00:00:19,920
relativement rares.

9
00:00:16,960 --> 00:00:22,160
Ceci signifie que lorsque les entreprises ont 

10
00:00:19,920 --> 00:00:24,320
besoin de quelqu'un disposant de ces compétences,

11
00:00:22,160 --> 00:00:26,560
les lois de l'offre et la demande font qu'une faible

12
00:00:24,320 --> 00:00:30,240
offre va d'autant plus valoriser ces compétences.

13
00:00:26,560 --> 00:00:31,840
Une autre raison est le fait que

14
00:00:30,240 --> 00:00:33,440
l'assembleur est essentiel à l'apprentissage

15
00:00:31,840 --> 00:00:35,120
du reverse engineering (la rétro-ingénierie).

16
00:00:33,440 --> 00:00:37,040
Que vous reversiiez

17
00:00:35,120 --> 00:00:37,840
des logiciels malveillants en tant qu'analyste

18
00:00:37,040 --> 00:00:40,160
malware

19
00:00:37,840 --> 00:00:40,879
ou des logiciels bénins en tant que chercheur

20
00:00:40,160 --> 00:00:42,480
de vulnérabilités.

21
00:00:40,879 --> 00:00:44,239
Dans les deux cas, il sera

22
00:00:42,480 --> 00:00:46,719
nécessaire de connaître tout ça.

23
00:00:44,239 --> 00:00:48,399
Le fait est que même si vous reversiez

24
00:00:46,719 --> 00:00:50,079
et trouviez des vulnérabilités logicielles,

25
00:00:48,399 --> 00:00:51,840
vous ne serez pas nécessairement en mesure

26
00:00:50,079 --> 00:00:54,079
d'exploiter ces vulnérabilités

27
00:00:51,840 --> 00:00:55,840
sans connaître d'assembleur. Parce que la recherche

28
00:00:54,079 --> 00:00:58,640
de vulnérabilités est une compétence en tant que telle

29
00:00:55,840 --> 00:01:00,239
et qu'exploiter les vulnérabilités en est une autre.

30
00:00:58,640 --> 00:01:01,440
Ou peut-être que vous souhaitez contribuer à rendre les systèmes

31
00:01:00,239 --> 00:01:03,359
plus sûrs.

32
00:01:01,440 --> 00:01:05,199
La connaissance de l'assembleur est essentielle afin

33
00:01:03,359 --> 00:01:06,720
de mettre les mains dans le cambouis d'un système

34
00:01:05,199 --> 00:01:08,320
afin de comprendre comment il fonctionne

35
00:01:06,720 --> 00:01:10,880
et ainsi pour comprendre comment le faire

36
00:01:08,320 --> 00:01:12,799
mieux fonctionner et de manière plus sûre.

37
00:01:10,880 --> 00:01:15,439
De nos jours, il est absolument sûr que

38
00:01:12,799 --> 00:01:17,200
les nouveaux mécanismes de sécurité sont intrinsèquement à la jointure

39
00:01:15,439 --> 00:01:19,119
entre hardware et software,

40
00:01:17,200 --> 00:01:20,960
et qu'ils vont fréquemment prendre la forme

41
00:01:19,119 --> 00:01:22,799
de nouvelles instructions d'assembleur

42
00:01:20,960 --> 00:01:25,040
qui devront être adoptées par les auteurs

43
00:01:22,799 --> 00:01:27,520
de compilateurs, systèmes d'exploitation, ou

44
00:01:25,040 --> 00:01:30,240
firmwares.

45
00:01:27,520 --> 00:01:32,079
Alors, personnellement, je compare très souvent 

46
00:01:30,240 --> 00:01:33,840
les différentes spécialisations et types

47
00:01:32,079 --> 00:01:36,720
de métiers que les ingénieurs en sécurité

48
00:01:33,840 --> 00:01:38,479
peuvent prendre comme des sortes

49
00:01:36,720 --> 00:01:39,520
de classes de métiers dans un jeu

50
00:01:38,479 --> 00:01:41,520
de rôles.

51
00:01:39,520 --> 00:01:43,600
J'ai tenté de creuser pour trouver les

52
00:01:41,520 --> 00:01:45,360
différentes classes de métier dans quelque chose comme

53
00:01:43,600 --> 00:01:46,079
Final Fantasy - les différents mages

54
00:01:45,360 --> 00:01:47,680
de couleur -

55
00:01:46,079 --> 00:01:49,200
et j'ai découvert que des mages particuliers

56
00:01:47,680 --> 00:01:50,880
étaient en fait éparpillés sur plusieurs différents

57
00:01:49,200 --> 00:01:52,079
jeux vidéo et n'ai pas pu y trouver de

58
00:01:50,880 --> 00:01:53,920
cohérence.

59
00:01:52,079 --> 00:01:55,920
Alors à la place, pardonnez-moi si

60
00:01:53,920 --> 00:01:58,079
je m'exprime en généralité à propos

61
00:01:55,920 --> 00:01:59,840
des couleurs de magie quand je parle

62
00:01:58,079 --> 00:02:02,000
à propos de classes de métier particulières.

63
00:01:59,840 --> 00:02:04,320
Alors vous seriez par exemple un mage

64
00:02:02,000 --> 00:02:06,880
blanc - un architecte de sécurité qui utilise

65
00:02:04,320 --> 00:02:09,440
ses pouvoirs de guérison afin de

66
00:02:06,880 --> 00:02:10,879
protéger et guérir les systèmes lorsqu'ils

67
00:02:09,440 --> 00:02:13,200
sont attaqués.

68
00:02:10,879 --> 00:02:14,319
Vous pourriez être une sorte de mage bleu

69
00:02:13,200 --> 00:02:17,200
 - qui utiliserait

70
00:02:14,319 --> 00:02:18,800
des pouvoirs de réplication afin de

71
00:02:17,200 --> 00:02:20,800
reverser des logiciels

72
00:02:18,800 --> 00:02:22,000
et finalement bien apprendre

73
00:02:20,800 --> 00:02:25,520
comment ils fonctionnent, si ce n'est mieux,

74
00:02:22,000 --> 00:02:26,800
que les ingénieurs originels. Vous pourriez être un

75
00:02:25,520 --> 00:02:29,680
mage vert

76
00:02:26,800 --> 00:02:30,560
- tel qu'un analyste de malware, qui sont

77
00:02:29,680 --> 00:02:33,280
fréquemment

78
00:02:30,560 --> 00:02:36,319
impliqués dans le bestiaire des

79
00:02:33,280 --> 00:02:38,720
malwares de l'internet d'aujourd'hui.

80
00:02:36,319 --> 00:02:40,239
Ou vous pourriez par exemple être un mage rouge

81
00:02:38,720 --> 00:02:42,080
qui, de par l'agressivité de sa puissance,

82
00:02:40,239 --> 00:02:44,239
ira chercher

83
00:02:42,080 --> 00:02:47,040
les vulnérabilités et essayera

84
00:02:44,239 --> 00:02:47,840
d'attaquer les systèmes. Enfin, vous pourriez

85
00:02:47,040 --> 00:02:50,800
par exemple

86
00:02:47,840 --> 00:02:52,480
être un mélange de mage noir Final Fantasy

87
00:02:50,800 --> 00:02:53,440
et d'un mage noir de

88
00:02:52,480 --> 00:02:55,040
Magic The Gathering

89
00:02:53,440 --> 00:02:56,720
qui invoquera fréquemment ses pouvoirs

90
00:02:55,040 --> 00:02:58,319
de nécromancie ou

91
00:02:56,720 --> 00:03:00,879
d'attaque de logiciel pour le tuer

92
00:02:58,319 --> 00:03:03,840
et ensuite le faire revivre afin de 

93
00:03:00,879 --> 00:03:04,400
lui servir pour ses propres intérêts. Ce que je

94
00:03:03,840 --> 00:03:06,319
veux dire, 

95
00:03:04,400 --> 00:03:07,920
c'est qu'en voyant les choses de ce côté-là, je me dis que

96
00:03:06,319 --> 00:03:09,519
c'est quand même vachement cool

97
00:03:07,920 --> 00:03:11,040
de voir tous les différents types de compétences que vous

98
00:03:09,519 --> 00:03:13,040
pouvez apprendre lorsque vous connaissez

99
00:03:11,040 --> 00:03:14,480
l'assembleur.

100
00:03:13,040 --> 00:03:16,480
Maintenant, une autre raison pour laquelle vous pourriez

101
00:03:14,480 --> 00:03:19,120
en avoir besoin est le fait que les meilleurs chercheurs

102
00:03:16,480 --> 00:03:21,280
vont très fréquemment impliquer de l'assembleur

103
00:03:19,120 --> 00:03:23,200
dans leurs présentations ou white papers.

104
00:03:21,280 --> 00:03:25,040
En général, lorsqu'ils essayent de faire passer

105
00:03:23,200 --> 00:03:25,680
un message sur le fonctionnement exact du

106
00:03:25,040 --> 00:03:27,120
comportement d'un système,

107
00:03:25,680 --> 00:03:28,879
afin de vous prouver la présence de

108
00:03:27,120 --> 00:03:30,159
vulnérabilités à tel endroit

109
00:03:28,879 --> 00:03:32,080
par exemple.

110
00:03:30,159 --> 00:03:33,680
Et donc : afin de devenir l'un de leur pair, vous

111
00:03:32,080 --> 00:03:34,640
aurez besoin de parler la même

112
00:03:33,680 --> 00:03:36,000
langue qu'eux.

113
00:03:34,640 --> 00:03:38,080
Vous devez comprendre ce qu'ils

114
00:03:36,000 --> 00:03:39,040
vont tenter de vous dire. En mettant

115
00:03:38,080 --> 00:03:41,200
un peu le nez dans quelques

116
00:03:39,040 --> 00:03:42,959
présentations des quelques dernières années

117
00:03:41,200 --> 00:03:45,840
d'une conférence en particulier,

118
00:03:42,959 --> 00:03:47,760
j'en ai trouvé quelques exemples. Par contre, 

119
00:03:45,840 --> 00:03:50,000
j'avoue que mon favoritisme se montre un peu parce que

120
00:03:47,760 --> 00:03:51,840
c'est une présentation de ma femme, datant de

121
00:03:50,000 --> 00:03:53,360
l'année dernière, qui utilisait de l'assembleur ARM

122
00:03:51,840 --> 00:03:54,879
afin de donner des explications à propos

123
00:03:53,360 --> 00:03:55,760
d'une vulnérabilité en particulier

124
00:03:54,879 --> 00:03:57,360
d'integer underflow,

125
00:03:55,760 --> 00:03:59,760
qu'elle avait trouvé dans la ROM d'une

126
00:03:57,360 --> 00:04:01,599
puce Bluetooth and comment elle était ainsi

127
00:03:59,760 --> 00:04:04,319
exploitable.

128
00:04:01,599 --> 00:04:05,040
Voici un exemple d'assembleur

129
00:04:04,319 --> 00:04:06,799
32-bit x86,

130
00:04:05,040 --> 00:04:08,799
lorsque quelqu'un faisait une rétrospective

131
00:04:06,799 --> 00:04:10,480
sur les différents mécanismes

132
00:04:08,799 --> 00:04:12,560
de chaînes d'exploit et comment il s'appliquaient

133
00:04:10,480 --> 00:04:14,640
à la vulnérabilité Stuxnet

134
00:04:12,560 --> 00:04:17,680
d'il y a quelques années.

135
00:04:14,640 --> 00:04:20,239
Ici, nous avions un assembleur 64-bit Intel

136
00:04:17,680 --> 00:04:22,320
qui visait la stack du pilote

137
00:04:20,239 --> 00:04:25,280
Wi-Fi d'Apple.

138
00:04:22,320 --> 00:04:26,639
Ici, nous avions à la fois de l'assembleur ARM et Intel

139
00:04:25,280 --> 00:04:29,040
datant d'une présentation sur le

140
00:04:26,639 --> 00:04:30,479
mécanisme que Windows utilise pour traduire

141
00:04:29,040 --> 00:04:34,960
les binaires Intel

142
00:04:30,479 --> 00:04:36,880
en exécutables ARM à la volée.

143
00:04:34,960 --> 00:04:39,440
Et finalement, vous pourriez avoir quelque chose comme

144
00:04:36,880 --> 00:04:43,040
ceci : l'assembleur MIPS utilisé dans pour attaquer

145
00:04:39,440 --> 00:04:43,440
un routeur Wi-Fi. Et vous aurez

146
00:04:43,040 --> 00:04:46,160
ensuite fréquemment

147
00:04:43,440 --> 00:04:47,040
des chercheurs qui vous amèneront dans

148
00:04:46,160 --> 00:04:49,199
des choses plus obscures comme ceci :

149
00:04:47,040 --> 00:04:50,320
de l'assembleur custom un peu

150
00:04:49,199 --> 00:04:53,840
moins commun mais

151
00:04:50,320 --> 00:04:56,320
qu'on pourrait trouver dans la couche

152
00:04:53,840 --> 00:04:57,360
réseau MAC (Media Access Control) 

153
00:04:56,320 --> 00:04:59,280
du Wi-Fi Broadcom

154
00:04:57,360 --> 00:05:01,120
qui est utilisée pour recevoir et traiter

155
00:04:59,280 --> 00:05:02,800
les paquets réseaux.

156
00:05:01,120 --> 00:05:05,520
Ou ceci, par exemple : de l'assembleur

157
00:05:02,800 --> 00:05:06,560
ARC, utilisé dans le contrôleur embarqué

158
00:05:05,520 --> 00:05:08,720
de Lenovo,

159
00:05:06,560 --> 00:05:11,600
qui a des fonctionnalités de sécurité additionnelles

160
00:05:08,720 --> 00:05:13,520
sur les ordis portables Lenovo.

161
00:05:11,600 --> 00:05:15,360
Voilà, c'étaient quelques petits exemples

162
00:05:13,520 --> 00:05:16,960
montrant comment les chercheurs essayent

163
00:05:15,360 --> 00:05:19,280
d'utiliser l'assembleur afin d'aller

164
00:05:16,960 --> 00:05:21,280
dans le vif du sujet des différentes présentations.

165
00:05:19,280 --> 00:05:22,880
Et finalement, si aucun de ces exemples

166
00:05:21,280 --> 00:05:24,960
ne vous a pas convaincu d'apprendre l'assembleur, 

167
00:05:22,880 --> 00:05:27,280
avec un peu de chances, ceci vous fera changer d'avis :

168
00:05:24,960 --> 00:05:29,120
afin de devenir un(e) hacker "correct", vous

169
00:05:27,280 --> 00:05:31,120
devriez être une personne curieuse,

170
00:05:29,120 --> 00:05:32,800
vous devriez essayer de comprendre comment

171
00:05:31,120 --> 00:05:35,199
les systèmes fonctionnent et pourquoi,

172
00:05:32,800 --> 00:05:36,560
et très fréquemment, l'assembleur est

173
00:05:35,199 --> 00:05:39,759
essentiel à la compréhension

174
00:05:36,560 --> 00:05:41,600
du comportement exact d'un logiciel et ses raisons.

175
00:05:39,759 --> 00:05:43,280
Et en fin de compte, comprendre

176
00:05:41,600 --> 00:05:44,720
comment un système fonctionne, lorsqu'on est

177
00:05:43,280 --> 00:05:49,360
une personne curieuse,

178
00:05:44,720 --> 00:05:49,360
c'est quand même vachement satisfaisant.

