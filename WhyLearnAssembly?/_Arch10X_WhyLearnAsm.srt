0
00:00:00,560 --> 00:00:04,560
Hello friends before you get started on

1
00:00:02,960 --> 00:00:06,480
this grand adventure perhaps you'd like

2
00:00:04,560 --> 00:00:08,080
to know why we, at OST, think you should

3
00:00:06,480 --> 00:00:09,920
learn assembly.

4
00:00:08,080 --> 00:00:12,000
Well, let's start with an easy one the

5
00:00:09,920 --> 00:00:13,840
first thing is that unlike some skill

6
00:00:12,000 --> 00:00:15,440
sets such as network security

7
00:00:13,840 --> 00:00:16,960
assembly language knowledge is

8
00:00:15,440 --> 00:00:19,920
relatively rare.

9
00:00:16,960 --> 00:00:22,160
This means that when companies actually

10
00:00:19,920 --> 00:00:24,320
need someone who has this skill set

11
00:00:22,160 --> 00:00:26,560
supply and demand dictates that the low

12
00:00:24,320 --> 00:00:30,240
supply is going to make the skill

13
00:00:26,560 --> 00:00:31,840
more valuable. Another reason is because

14
00:00:30,240 --> 00:00:33,440
assembly is essential to learning

15
00:00:31,840 --> 00:00:35,120
reverse engineering

16
00:00:33,440 --> 00:00:37,040
whether you're doing reverse engineering

17
00:00:35,120 --> 00:00:37,840
of malicious software as a malware

18
00:00:37,040 --> 00:00:40,160
analyst

19
00:00:37,840 --> 00:00:40,879
or benign software as a vulnerability

20
00:00:40,160 --> 00:00:42,480
hunter.

21
00:00:40,879 --> 00:00:44,239
In either case, it's going to be

22
00:00:42,480 --> 00:00:46,719
necessary to know this,

23
00:00:44,239 --> 00:00:48,399
but the thing is even if you reverse

24
00:00:46,719 --> 00:00:50,079
engineer and find vulnerabilities in

25
00:00:48,399 --> 00:00:51,840
software you won't necessarily be able

26
00:00:50,079 --> 00:00:54,079
to exploit those vulnerabilities

27
00:00:51,840 --> 00:00:55,840
unless you know assembly because finding

28
00:00:54,079 --> 00:00:58,640
vulnerabilities is one skill set

29
00:00:55,840 --> 00:01:00,239
and exploiting them is another or

30
00:00:58,640 --> 00:01:01,440
perhaps you want to help make systems

31
00:01:00,239 --> 00:01:03,359
more secure

32
00:01:01,440 --> 00:01:05,199
knowledge of assembly is essential to

33
00:01:03,359 --> 00:01:06,720
digging down into the guts of the system

34
00:01:05,199 --> 00:01:08,320
to understand how it works

35
00:01:06,720 --> 00:01:10,880
so that you can understand how to make

36
00:01:08,320 --> 00:01:12,799
it work better and more securely.

37
00:01:10,880 --> 00:01:15,439
These days it's definitely the case that

38
00:01:12,799 --> 00:01:17,200
new security mechanisms are deeply tied

39
00:01:15,439 --> 00:01:19,119
between hardware and software

40
00:01:17,200 --> 00:01:20,960
and these will frequently take the form

41
00:01:19,119 --> 00:01:22,799
of new assembly instructions

42
00:01:20,960 --> 00:01:25,040
that have to be adopted by the authors

43
00:01:22,799 --> 00:01:27,520
of compilers operating systems or

44
00:01:25,040 --> 00:01:30,240
firmware.

45
00:01:27,520 --> 00:01:32,079
So, personally, I frequently think of the

46
00:01:30,240 --> 00:01:33,840
different specializations and job

47
00:01:32,079 --> 00:01:36,720
classes that security

48
00:01:33,840 --> 00:01:38,479
engineers can increasingly take as sort

49
00:01:36,720 --> 00:01:39,520
of like job classes in a role-playing

50
00:01:38,479 --> 00:01:41,520
game,

51
00:01:39,520 --> 00:01:43,600
and I went and I tried to dig up the

52
00:01:41,520 --> 00:01:45,360
various job classes in something like

53
00:01:43,600 --> 00:01:46,079
Final Fantasy - the different colored

54
00:01:45,360 --> 00:01:47,680
mages -

55
00:01:46,079 --> 00:01:49,200
and I found that the particular mages

56
00:01:47,680 --> 00:01:50,880
were spread across a bunch of different

57
00:01:49,200 --> 00:01:52,079
video games and so I couldn't find any

58
00:01:50,880 --> 00:01:53,920
consistency there.

59
00:01:52,079 --> 00:01:55,920
So instead, you'll have to forgive me if

60
00:01:53,920 --> 00:01:58,079
I speak in generalisms about

61
00:01:55,920 --> 00:01:59,840
magic colors when I talk about these

62
00:01:58,079 --> 00:02:02,000
particular job classes.

63
00:01:59,840 --> 00:02:04,320
So for instance, you might be a white

64
00:02:02,000 --> 00:02:06,880
mage - a security architect who's using

65
00:02:04,320 --> 00:02:09,440
their healing powers in order to

66
00:02:06,880 --> 00:02:10,879
protect and heal systems when they

67
00:02:09,440 --> 00:02:13,200
become attacked.

68
00:02:10,879 --> 00:02:14,319
You might be something like a blue mage

69
00:02:13,200 --> 00:02:17,200
 - using the

70
00:02:14,319 --> 00:02:18,800
mirror or replication powers in order to

71
00:02:17,200 --> 00:02:20,800
reverse engineer software

72
00:02:18,800 --> 00:02:22,000
ultimately learning the software just as

73
00:02:20,800 --> 00:02:25,520
well, if not better,

74
00:02:22,000 --> 00:02:26,800
as the original engineers. You could be a

75
00:02:25,520 --> 00:02:29,680
green mage

76
00:02:26,800 --> 00:02:30,560
- such as malware analysts who are

77
00:02:29,680 --> 00:02:33,280
frequently

78
00:02:30,560 --> 00:02:36,319
highly entangled in the bestiary that is

79
00:02:33,280 --> 00:02:38,720
malicious software on the internet today,

80
00:02:36,319 --> 00:02:40,239
or you could for instance be a red mage -

81
00:02:38,720 --> 00:02:42,080
with the aggressive

82
00:02:40,239 --> 00:02:44,239
power set which is going after

83
00:02:42,080 --> 00:02:47,040
vulnerabilities and trying to

84
00:02:44,239 --> 00:02:47,840
attack software. Or, finally, here we have

85
00:02:47,040 --> 00:02:50,800
an example

86
00:02:47,840 --> 00:02:52,480
of proper mix of final fantasy black

87
00:02:50,800 --> 00:02:53,440
mage and a magic the gathering black

88
00:02:52,480 --> 00:02:55,040
mage -

89
00:02:53,440 --> 00:02:56,720
frequently invoking the powers of

90
00:02:55,040 --> 00:02:58,319
necromancy or

91
00:02:56,720 --> 00:03:00,879
attacking the software and killing it

92
00:02:58,319 --> 00:03:03,840
and raising it back up in order to

93
00:03:00,879 --> 00:03:04,400
make it do what they want it to do. So I

94
00:03:03,840 --> 00:03:06,319
mean,

95
00:03:04,400 --> 00:03:07,920
when I look at things this way, I think

96
00:03:06,319 --> 00:03:09,519
it's pretty freaking awesome

97
00:03:07,920 --> 00:03:11,040
the different types of skill sets you

98
00:03:09,519 --> 00:03:13,040
can learn when you know assembly

99
00:03:11,040 --> 00:03:14,480
language.

100
00:03:13,040 --> 00:03:16,480
Now, another reason why you might need to

101
00:03:14,480 --> 00:03:19,120
know it is because very frequently the

102
00:03:16,480 --> 00:03:21,280
top researchers will invoke assembly

103
00:03:19,120 --> 00:03:23,200
in their presentations or white papers

104
00:03:21,280 --> 00:03:25,040
usually when they're trying to make some

105
00:03:23,200 --> 00:03:25,680
point about how exactly the system

106
00:03:25,040 --> 00:03:27,120
behaved,

107
00:03:25,680 --> 00:03:28,879
in order to prove to you that for

108
00:03:27,120 --> 00:03:30,159
instance the vulnerability was there or

109
00:03:28,879 --> 00:03:32,080
whatnot

110
00:03:30,159 --> 00:03:33,680
And so in order to become their peer, you

111
00:03:32,080 --> 00:03:34,640
need to be able to speak the same

112
00:03:33,680 --> 00:03:36,000
language as them.

113
00:03:34,640 --> 00:03:38,080
You need to understand what they're

114
00:03:36,000 --> 00:03:39,040
trying to say to you. So I did a quick

115
00:03:38,080 --> 00:03:41,200
skim across

116
00:03:39,040 --> 00:03:42,959
some talks from the last couple years at

117
00:03:41,200 --> 00:03:45,840
one particular conference

118
00:03:42,959 --> 00:03:47,760
and found some examples of this. Now here,

119
00:03:45,840 --> 00:03:50,000
my nepotism is showing because

120
00:03:47,760 --> 00:03:51,840
this is my wife's presentation from last

121
00:03:50,000 --> 00:03:53,360
year using arm assembly

122
00:03:51,840 --> 00:03:54,879
in order to make the point about a

123
00:03:53,360 --> 00:03:55,760
particular integer underflow

124
00:03:54,879 --> 00:03:57,360
vulnerability

125
00:03:55,760 --> 00:03:59,760
which she had found in the rom of a

126
00:03:57,360 --> 00:04:01,599
bluetooth chip and how it was therefore

127
00:03:59,760 --> 00:04:04,319
exploitable.

128
00:04:01,599 --> 00:04:05,040
Here's an example of some 32-bit x86

129
00:04:04,319 --> 00:04:06,799
assembly,

130
00:04:05,040 --> 00:04:08,799
when someone was doing a retrospective

131
00:04:06,799 --> 00:04:10,480
on the various exploit chain

132
00:04:08,799 --> 00:04:12,560
mechanisms and how they could be filled

133
00:04:10,480 --> 00:04:14,640
back in for the stuxnet vulnerability

134
00:04:12,560 --> 00:04:17,680
from a number of years ago.

135
00:04:14,640 --> 00:04:20,239
Here was some 64-bit intel assembly

136
00:04:17,680 --> 00:04:22,320
about digging into the apple wi-fi

137
00:04:20,239 --> 00:04:25,280
driver stack.

138
00:04:22,320 --> 00:04:26,639
Here was both arm and intel assembly

139
00:04:25,280 --> 00:04:29,040
when someone was talking about the

140
00:04:26,639 --> 00:04:30,479
mechanism that windows uses to translate

141
00:04:29,040 --> 00:04:34,960
intel binaries

142
00:04:30,479 --> 00:04:36,880
over to arm executables on the fly.

143
00:04:34,960 --> 00:04:39,440
And then you might have something like

144
00:04:36,880 --> 00:04:43,040
this: the mips assembly used to attack

145
00:04:39,440 --> 00:04:43,440
a wi-fi router. And frequently you'll

146
00:04:43,040 --> 00:04:46,160
have

147
00:04:43,440 --> 00:04:47,040
researchers get into more obscure things

148
00:04:46,160 --> 00:04:49,199
such as this: 

149
00:04:47,040 --> 00:04:50,320
custom assembly language infrequently

150
00:04:49,199 --> 00:04:53,840
found but

151
00:04:50,320 --> 00:04:56,320
found inside of the broadcom wi-fi

152
00:04:53,840 --> 00:04:57,360
mac media access control physical

153
00:04:56,320 --> 00:04:59,280
hardware layer

154
00:04:57,360 --> 00:05:01,120
that it uses to receive and process

155
00:04:59,280 --> 00:05:02,800
packets.

156
00:05:01,120 --> 00:05:05,520
Or this, for instance, which is arc

157
00:05:02,800 --> 00:05:06,560
assembly as used in the lenovo embedded

158
00:05:05,520 --> 00:05:08,720
controller

159
00:05:06,560 --> 00:05:11,600
which has additional security features

160
00:05:08,720 --> 00:05:13,520
and functionality on lenovo laptops.

161
00:05:11,600 --> 00:05:15,360
So, those were just some quick examples

162
00:05:13,520 --> 00:05:16,960
of how researchers have

163
00:05:15,360 --> 00:05:19,280
tried to use assembly in order to make

164
00:05:16,960 --> 00:05:21,280
their point in various talks.

165
00:05:19,280 --> 00:05:22,880
And finally, if none of the rest of that

166
00:05:21,280 --> 00:05:24,960
convinced you of why you should learn

167
00:05:22,880 --> 00:05:27,280
assembly, hopefully this will:

168
00:05:24,960 --> 00:05:29,120
in order to be a proper hacker you

169
00:05:27,280 --> 00:05:31,120
should be curious

170
00:05:29,120 --> 00:05:32,800
you should want to understand how

171
00:05:31,120 --> 00:05:35,199
systems work and why,

172
00:05:32,800 --> 00:05:36,560
and very frequently assembly language is

173
00:05:35,199 --> 00:05:39,759
essential to understanding

174
00:05:36,560 --> 00:05:41,600
exactly how software is behaving and why

175
00:05:39,759 --> 00:05:43,280
and at the end of the day understanding

176
00:05:41,600 --> 00:05:44,720
how the system works, when you're very

177
00:05:43,280 --> 00:05:49,360
curious person,

178
00:05:44,720 --> 00:05:49,360
is extremely satisfying.

