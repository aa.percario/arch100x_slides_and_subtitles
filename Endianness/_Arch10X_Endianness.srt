0
00:00:00,160 --> 00:00:03,520
all right so let's briefly talk about

1
00:00:01,760 --> 00:00:05,120
endianness because this is something

2
00:00:03,520 --> 00:00:06,799
that's confusing to folks who are

3
00:00:05,120 --> 00:00:08,240
digging deeper than the C level for

4
00:00:06,799 --> 00:00:10,400
the first time

5
00:00:08,240 --> 00:00:12,559
so the term endianness comes from

6
00:00:10,400 --> 00:00:14,160
Jonathan Swift's Gulliver's Travels a

7
00:00:12,559 --> 00:00:16,240
book back in the day

8
00:00:14,160 --> 00:00:17,279
in which people go to war over the

9
00:00:16,240 --> 00:00:19,760
correct way to eat

10
00:00:17,279 --> 00:00:21,680
soft boiled eggs whether the little end

11
00:00:19,760 --> 00:00:23,119
should be cracked first or the big end

12
00:00:21,680 --> 00:00:25,000
should be cracked first

13
00:00:23,119 --> 00:00:26,160
this of course was a satire about the

14
00:00:25,000 --> 00:00:28,720
ridiculousness

15
00:00:26,160 --> 00:00:31,279
of England and France and their various

16
00:00:28,720 --> 00:00:33,520
ongoing religious wars and whatnot

17
00:00:31,279 --> 00:00:34,960
well little endianness term was

18
00:00:33,520 --> 00:00:39,120
introduced to refer to

19
00:00:34,960 --> 00:00:41,840
how you store a value like 0x12345678

20
00:00:39,120 --> 00:00:44,000
into RAM and specifically little

21
00:00:41,840 --> 00:00:46,320
endianness means that the little end or

22
00:00:44,000 --> 00:00:47,039
the least significant byte is going to

23
00:00:46,320 --> 00:00:49,280
be stored

24
00:00:47,039 --> 00:00:50,239
at the lowest address so 0x123

25
00:00:49,280 --> 00:00:54,000
45678

26
00:00:50,239 --> 00:00:57,039
would be stored as 0x78

27
00:00:54,000 --> 00:01:00,559
at the lowest address then 56

28
00:00:57,039 --> 00:01:02,640
34 12. so Intel is little endian and

29
00:01:00,559 --> 00:01:05,680
consequently if you look at values

30
00:01:02,640 --> 00:01:07,280
in RAM it'll look sort of backwards the

31
00:01:05,680 --> 00:01:09,760
way that it would naturally be

32
00:01:07,280 --> 00:01:12,159
represented in hex

33
00:01:09,760 --> 00:01:13,600
big endian therefore is the opposite so

34
00:01:12,159 --> 00:01:15,680
you store 0x12345678

35
00:01:13,600 --> 00:01:17,439
in RAM big end or most

36
00:01:15,680 --> 00:01:19,600
significant byte first

37
00:01:17,439 --> 00:01:21,439
so 0x12345678

38
00:01:19,600 --> 00:01:22,400
if you were looking at it one byte

39
00:01:21,439 --> 00:01:25,040
at a time

40
00:01:22,400 --> 00:01:26,159
so network traffic is typically sent in

41
00:01:25,040 --> 00:01:29,040
big endian order

42
00:01:26,159 --> 00:01:30,560
so if you you know do the man byte order

43
00:01:29,040 --> 00:01:32,720
you'll see the posix

44
00:01:30,560 --> 00:01:34,479
network to host ordering functions like

45
00:01:32,720 --> 00:01:37,520
network to

46
00:01:34,479 --> 00:01:38,159
host order network to host long network

47
00:01:37,520 --> 00:01:40,640
to host

48
00:01:38,159 --> 00:01:43,119
short and host to network long host to

49
00:01:40,640 --> 00:01:45,360
network short and so forth

50
00:01:43,119 --> 00:01:46,320
so many RISC systems when they first

51
00:01:45,360 --> 00:01:49,600
were starting out

52
00:01:46,320 --> 00:01:50,960
were big endian order and eventually

53
00:01:49,600 --> 00:01:52,880
they changed them to be

54
00:01:50,960 --> 00:01:56,079
configurable to either be big endian or

55
00:01:52,880 --> 00:01:58,399
little which is referred to as bi-endian

56
00:01:56,079 --> 00:01:59,200
whereas arm started out little endian

57
00:01:58,399 --> 00:02:02,960
and now

58
00:01:59,200 --> 00:02:03,759
is bi-endian so a couple of things that

59
00:02:02,960 --> 00:02:06,320
frequently trip

60
00:02:03,759 --> 00:02:08,879
people up the first is that endianness

61
00:02:06,320 --> 00:02:11,039
only applies to the memory storage

62
00:02:08,879 --> 00:02:12,239
of values not to registers whenever

63
00:02:11,039 --> 00:02:14,800
you're looking at a register

64
00:02:12,239 --> 00:02:16,720
it'll be represented in big endian order

65
00:02:14,800 --> 00:02:17,440
meaning the most significant byte is to

66
00:02:16,720 --> 00:02:19,840
the left

67
00:02:17,440 --> 00:02:22,160
least significant byte is to the right

68
00:02:19,840 --> 00:02:22,879
furthermore endianness only applies to

69
00:02:22,160 --> 00:02:25,360
bytes

70
00:02:22,879 --> 00:02:27,040
not bits so frequently you know folks

71
00:02:25,360 --> 00:02:28,800
will be confused and say oh

72
00:02:27,040 --> 00:02:30,640
should like I flip around the bit

73
00:02:28,800 --> 00:02:32,000
ordering for endianness and the answer

74
00:02:30,640 --> 00:02:33,680
is no you should not

75
00:02:32,000 --> 00:02:36,000
when you're looking at memory not

76
00:02:33,680 --> 00:02:37,760
registers and you see a byte

77
00:02:36,000 --> 00:02:39,200
it will always still have the least

78
00:02:37,760 --> 00:02:40,879
significant bit

79
00:02:39,200 --> 00:02:44,480
on the right hand side and the most

80
00:02:40,879 --> 00:02:46,319
significant bit on the left hand side

81
00:02:44,480 --> 00:02:47,920
so this was the fundamental data types

82
00:02:46,319 --> 00:02:49,280
that's in the Intel manual it's in the

83
00:02:47,920 --> 00:02:52,000
refresher section

84
00:02:49,280 --> 00:02:53,200
so a character is going to have you know

85
00:02:52,000 --> 00:02:55,680
a single byte so

86
00:02:53,200 --> 00:02:56,319
it has no issue with endianness, whereas

87
00:02:55,680 --> 00:02:58,879
a short

88
00:02:56,319 --> 00:03:00,239
because it's multi-byte the endianness

89
00:02:58,879 --> 00:03:01,519
will be stored either you know least

90
00:03:00,239 --> 00:03:04,239
significant byte first

91
00:03:01,519 --> 00:03:05,680
most significant byte next if it's

92
00:03:04,239 --> 00:03:08,000
a little endian like Intel

93
00:03:05,680 --> 00:03:10,080
or the opposite if it's big endian but

94
00:03:08,000 --> 00:03:11,200
the bits within those bytes are still

95
00:03:10,080 --> 00:03:13,360
represented as

96
00:03:11,200 --> 00:03:16,239
least significant to the right most

97
00:03:13,360 --> 00:03:19,040
significant to the left

98
00:03:16,239 --> 00:03:20,879
so here's an example of how that looks I

99
00:03:19,040 --> 00:03:22,560
will frequently in this class be

100
00:03:20,879 --> 00:03:23,920
drawing low addresses low and high

101
00:03:22,560 --> 00:03:25,840
addresses is high

102
00:03:23,920 --> 00:03:27,360
you have to be flexible to be able to

103
00:03:25,840 --> 00:03:29,920
deal with anyone drawing

104
00:03:27,360 --> 00:03:31,680
things in any particular direction but

105
00:03:29,920 --> 00:03:32,799
this will get into the stack section

106
00:03:31,680 --> 00:03:35,360
that we learn about

107
00:03:32,799 --> 00:03:37,280
later on that's just sort of how it was

108
00:03:35,360 --> 00:03:38,480
taught in my computer architecture class

109
00:03:37,280 --> 00:03:39,360
and that's how I'm continuing to

110
00:03:38,480 --> 00:03:42,799
propagate it

111
00:03:39,360 --> 00:03:45,599
so low low high high so like I said

112
00:03:42,799 --> 00:03:46,879
registers are always sort of shown in a

113
00:03:45,599 --> 00:03:48,959
big endian form

114
00:03:46,879 --> 00:03:50,720
with the most significant byte to the

115
00:03:48,959 --> 00:03:51,519
left and the least significant byte to

116
00:03:50,720 --> 00:03:53,360
the right

117
00:03:51,519 --> 00:03:55,120
so whether it's big endian or little

118
00:03:53,360 --> 00:03:56,080
endian the register is always going to

119
00:03:55,120 --> 00:03:58,239
be shown in

120
00:03:56,080 --> 00:04:00,000
big endian form but then the difference

121
00:03:58,239 --> 00:04:02,000
between big ending and little endian

122
00:04:00,000 --> 00:04:03,280
is the order in which they go into

123
00:04:02,000 --> 00:04:05,680
memory addresses

124
00:04:03,280 --> 00:04:07,680
so in a big endian architecture the big

125
00:04:05,680 --> 00:04:09,599
end will go to the lowest address

126
00:04:07,680 --> 00:04:11,439
whereas in a little endian architecture

127
00:04:09,599 --> 00:04:12,319
the little end will go to the lowest

128
00:04:11,439 --> 00:04:14,080
address

129
00:04:12,319 --> 00:04:16,160
so consequently it sort of looks

130
00:04:14,080 --> 00:04:18,160
backwards when you're looking at it

131
00:04:16,160 --> 00:04:19,919
in memory now the most common way that

132
00:04:18,160 --> 00:04:22,240
you're going to see it in tooling

133
00:04:19,919 --> 00:04:24,639
is something like this typically it's

134
00:04:22,240 --> 00:04:27,199
written in sort of the english style

135
00:04:24,639 --> 00:04:27,680
of we're going to read it from left to

136
00:04:27,199 --> 00:04:30,400
right

137
00:04:27,680 --> 00:04:32,560
top to bottom so for instance if you're

138
00:04:30,400 --> 00:04:34,880
looking at registers we said registers

139
00:04:32,560 --> 00:04:36,639
the endianness is always big endian so

140
00:04:34,880 --> 00:04:38,880
you know 112233

141
00:04:36,639 --> 00:04:41,919
etc the low address

142
00:04:38,880 --> 00:04:42,800
side here the least significant bytes

143
00:04:41,919 --> 00:04:45,120
here and the

144
00:04:42,800 --> 00:04:46,240
most significant bytes here but when you

145
00:04:45,120 --> 00:04:48,960
take and you put that

146
00:04:46,240 --> 00:04:49,840
into memory it'll be flipped around so

147
00:04:48,960 --> 00:04:52,960
1122

148
00:04:49,840 --> 00:04:54,840
33 turns into 8877

149
00:04:52,960 --> 00:04:57,759
66

150
00:04:54,840 --> 00:05:01,600
etc but this is only again

151
00:04:57,759 --> 00:05:03,680
for memory bytes and if you start asking

152
00:05:01,600 --> 00:05:04,880
your debugger to represent it instead of

153
00:05:03,680 --> 00:05:07,039
1 byte at a time

154
00:05:04,880 --> 00:05:08,400
but in 4 bytes at a time or 8

155
00:05:07,039 --> 00:05:10,960
bytes at a time

156
00:05:08,400 --> 00:05:11,440
the debugger will consequently flip it

157
00:05:10,960 --> 00:05:14,479
around

158
00:05:11,440 --> 00:05:15,840
and represent it big endian so if we for

159
00:05:14,479 --> 00:05:16,639
instance have 0x12345

160
00:05:15,840 --> 00:05:19,440
678

161
00:05:16,639 --> 00:05:20,639
87654321

162
00:05:19,440 --> 00:05:24,080
if you display it

163
00:05:20,639 --> 00:05:26,479
as you know 4 bytes at a time

164
00:05:24,080 --> 00:05:27,759
then the first 4 bytes which are

165
00:05:26,479 --> 00:05:30,240
going to be these little

166
00:05:27,759 --> 00:05:32,240
end right here are going to be you know

167
00:05:30,240 --> 00:05:34,479
87654321

168
00:05:32,240 --> 00:05:36,479
that's the least significant 4 bytes

169
00:05:34,479 --> 00:05:38,960
flipped around to be big endian

170
00:05:36,479 --> 00:05:40,720
followed by the next most significant

171
00:05:38,960 --> 00:05:41,520
4 bytes flipped around to be a big

172
00:05:40,720 --> 00:05:44,080
endian

173
00:05:41,520 --> 00:05:46,000
and 123456 so like

174
00:05:44,080 --> 00:05:48,240
that it's still kind of backwards

175
00:05:46,000 --> 00:05:49,440
from the interpretation of the full

176
00:05:48,240 --> 00:05:51,120
8 byte value

177
00:05:49,440 --> 00:05:52,800
so if you ask the debugger please

178
00:05:51,120 --> 00:05:54,560
display an 8 bytes at a time then

179
00:05:52,800 --> 00:05:56,880
you'll start seeing the match-up

180
00:05:54,560 --> 00:05:58,479
of exactly what you see in memory

181
00:05:56,880 --> 00:05:59,360
exactly corresponds to what you see in

182
00:05:58,479 --> 00:06:01,520
the register

183
00:05:59,360 --> 00:06:03,360
because you've corresponded the big

184
00:06:01,520 --> 00:06:06,319
endianness of the memory display to the

185
00:06:03,360 --> 00:06:08,319
big endianness of the register display

186
00:06:06,319 --> 00:06:09,440
and here's an example of the same thing

187
00:06:08,319 --> 00:06:12,960
in gdb

188
00:06:09,440 --> 00:06:14,960
so least significant byte this way

189
00:06:12,960 --> 00:06:16,720
and if you display it 4 at a time

190
00:06:14,960 --> 00:06:20,479
then it'll flip it around and so

191
00:06:16,720 --> 00:06:23,600
0x54889e5 0x548

192
00:06:20,479 --> 00:06:24,960
89e5 so

193
00:06:23,600 --> 00:06:26,560
this is just you know the brief

194
00:06:24,960 --> 00:06:27,840
introduction to endianness you really

195
00:06:26,560 --> 00:06:29,120
only need to know about it when you're

196
00:06:27,840 --> 00:06:32,400
looking at

197
00:06:29,120 --> 00:06:34,000
values byte-wise in memory or in

198
00:06:32,400 --> 00:06:36,319
you know increments that are smaller

199
00:06:34,000 --> 00:06:38,160
than the particular register size for

200
00:06:36,319 --> 00:06:41,759
instance which is going to be reading in

201
00:06:38,160 --> 00:06:41,759
or writing out the value

